<?php
   
$config = parse_ini_file('config.ini', true, INI_SCANNER_TYPED );

//if (strlen($config[GeneralSetting][iniLocation]) > 0) {
//    $config = parse_ini_file($config[GeneralSetting][iniLocation], true);
//} else {
//    $config = parse_ini_file("config.ini", true);
//}


#Mapping Name from HTML fields
$emailName = $config[MailInOne_Settings][Mapping][EMAIL];
$anrede = $config[MailInOne_Settings][Mapping][SALUTATION];
$vorname = $config[MailInOne_Settings][Mapping][FIRSTNAME];
$nachname = $config[MailInOne_Settings][Mapping][LASTNAME];
$adresse = $config[MailInOne_Settings][Mapping][ADDRESS];
$plz = $config[MailInOne_Settings][Mapping][ZIP];
$ort = $config[MailInOne_Settings][Mapping][CITY];
$telefon = $config[MailInOne_Settings][Mapping][TELEFON];
$quelle = $config[MailInOne_Settings][Mapping][Quelle];
$typ = $config[MailInOne_Settings][Mapping][Typ];
$segment = $config[MailInOne_Settings][Mapping][Segment];
$ts = $config[MailInOne_Settings][Mapping][trafficsource];
$hubspotcontact = $config[MailInOne_Settings][Mapping][HubspotContactID];
$hubspotdeal = $config[MailInOne_Settings][Mapping][HubspotDealID];
$vertrieb = $config[MailInOne_Settings][Mapping][Vertriebsmitarbeiter];
$vertriebid = $config[MailInOne_Settings][Mapping][Vertriebsmitarbeiterid];
$url = $config[MailInOne_Settings][Mapping][URL];
$ip = $config[MailInOne_Settings][Mapping][IP];

$constts = $config[MailInOne_Settings][Constants][ts];
$consttyp = $config[MailInOne_Settings][Constants][Typ];
$constsegment = $config[MailInOne_Settings][Constants][Segment];
$constquelle = $config[MailInOne_Settings][Constants][Quelle];
        
#AssignmentManager
$assignmentstatus = $config[AssignmentManager][Status];

#GeneralSetting
$supportprefilling = ( $config[GeneralSetting][SupportPreFilling] == (true))? true : '';
$cutcopypaste = ($config[GeneralSetting][CutCopyandPaste] == (true))? true : '';
$updateexistingrecord = ($config[GeneralSetting][UpdateExistingRecord] == (true))? true : '';
$redirectstatus = $config[GeneralSetting][Redirect][RedirectStatus];
$redirectpath = $config[GeneralSetting][Redirect][RedirectPath];
$redirecttime = $config[GeneralSetting][Redirect][RedirectTime];
$successpage = $config[GeneralSetting][SuccessPage];
$keepurlparameters = ($config[GeneralSetting][KeepURLParameter] == (true))? true : '';

        
#Hubspot
$hubspotsync = ($config[HubSpotCredentials][Sync] == (true))? true : '';
$hubspotkey = $config[HubSpotCredentials][hubapi];
$hubspotdealname = $config[HubSpotDeal][dealname];
$hubspotdealamount = $config[HubSpotDeal][amount];
$hubspotdealsegement = $config[HubSpotDeal][segment];
$hubspotdealtype = $config[HubSpotDeal][dealtype];
$hubspotdealstage = $config[HubSpotDeal][dealstage];
$hubspotdealclosedate = $config[HubSpotDeal][closedate];
$hubspotdealpipeline = $config[HubSpotDeal][pipeline];
$hubspotdealqualitaet = $config[HubSpotDeal][qualitaet];
        

#Mail-in-One
$maileonstatus = ( $config[MailInOne_Settings][Sync] == (true))? true : '';
$maileonkey = $config[MailInOne_Settings][Mapikey];
$maileonconfig = array(
    'BASE_URI' => 'https://api.maileon.com/1.0',
    'API_KEY' => $maileonkey,
    'THROW_EXCEPTION' => true,
    'TIMEOUT' => 60,
    'DEBUG' => 'false' // NEVER enable on production
);
$maileonconstant = $config[MailInOne_Settings][Constants];
$maileondoikey = $config[MailInOne_Settings][DOIKey];

#TackingTools
$facebookpixel = $config[TrackingTools][FacebookPixel];
$googleKey = $config[TrackingTools][GoogleAPIKey];
$googleAnalyticsKey = $config[TrackingTools][GoogleAnalytics];


#Postal code
$plzstatus = ($config[Postal_Code][Autofill] == (true))? true : '';
$plzerrcolor = $config[Postal_Code][ErrorMsg_Color];
$plzerr = $config[Postal_Code][ErrorMsg];

#Splittest
$splittestname = $config[Splittest][Name];
$splitteststatus = ($config[Splittest][RunSplittest]  == (true))? true : '';
$splittestvarients = $config[Splittest][NumberOfVariants];       

#Telefon
$telefonstatus = ($config[Telefon][Status] == (true))? true : '';
$telefonerrcolor = $config[Telefon][ErrorMsg_Color];
$telefonerr = $config[Telefon][ErrorMsg];



