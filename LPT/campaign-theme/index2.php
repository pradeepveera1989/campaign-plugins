<?php

	/* ______________________________________________________________________________________________________________________________________________
	 *
	 MAIL IN ONE - STIMAWELL - 1807 - Aktion 49€ - Launch Kampagne
	 * _______________________________________________________________________________________________________________________________________________
	 */

	// Get the config data from config.ini       

	
?>

<?php
session_start();
include 'config.php';

if ($config[Splittest][RunSplittest] && !$config[Varient][VarientFile]) {

    # Create folders for the Splittest 
    # Executed only for the 1st time.
    $spTestName = $config[Splittest][Name];
    $varients = $config[Splittest][NumberOfVariants];
    $spTestName = $config[Splittest][Name];
    $dir = getcwd();

    while ($varients > 0) {
        $folder = $spTestName . '/V' . $varients . '/';
        if (!is_dir($folder)) {
            mkdir($folder, 0777, true);
        }
        copyDirectory($dir, $folder, $spTestName);
        if (!modifyFile($folder . 'config.ini', $config[Splittest][iniLocation])) {

            #something went wrong
            return false;
        }
        $varients = $varients - 1;
    }

    #Create a new object for Split Test 
    $spTest = new splitTest($spTestName, $config[Splittest][NumberOfVariants]);
    if (!($spTest->tableExists())) {
        # Excutes only once while creating the splitTest
        $spTest->createTable();
        $spTestIndex = 1;
    } else {
        #Get the current value of split Test index
        $spTest->getCurrentIndex();
    }

    $spTestNewIndex = $spTest->getNewIndex($spTestIndex);
    $spTestNewLink = $spTestName . '/V' . $spTestNewIndex . '/index.php';

    #Get a new splitTest index
    if ($spTestNewLink) {
        # Divides the URL and appends the new splittest index and appends the parameters.
        $url_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        $php_parts = explode('/', $_SERVER['REQUEST_URI']);
        $index = $php_parts[sizeof($php_parts) - 1];
        array_pop($php_parts);
        $link = implode($php_parts, "/");

        # Generate a new validate link.
        $redirectURL = "https://" . $_SERVER['HTTP_HOST'] . $link . "/" . $spTestNewLink . "?" . $url_parts[1];


        #Finally redirects the URL to new Link.
        echo '<script type="text/javascript">';
        echo "window.top.location='" . $redirectURL . "'";
        echo '</script>';
    }
} else {

    //vorname  
    $standard_01 = $_GET["1"];
    //nachname
    $standard_02 = $_GET["2"];
    //email
    $email_02 = $_GET["3"];
    //plz
    $standard_04 = $_GET["4"];
    //ort
    $standard_05 = $_GET["5"];
    //telefon
    $custom_03 = $_GET["6"];
    //trafficsource
    $ts = $_GET["trafficsource"];

    $standard_01 = str_replace(' ', '+', $standard_01);
    $standard_02 = str_replace(' ', '+', $standard_02);
    $email_02 = str_replace(' ', '+', $email_02);
    $standard_04 = str_replace(' ', '+', $standard_04);
    $standard_05 = str_replace(' ', '+', $standard_05);
    $custom_03 = str_replace(' ', '+', $custom_03);

    if ($config[GeneralSetting][SupportPreFilling]) {

        $standard_001 = base64_decode($standard_01);
        $standard_002 = base64_decode($standard_02);
        $email_002 = base64_decode($email_02);
        $standard_004 = base64_decode($standard_04);
        $standard_005 = base64_decode($standard_05);
        $custom_003 = base64_decode($custom_03);
    }

    if (isset($_GET['contactid'])) {
        $contactid = $_GET['contactid'];
    } elseif (isset($_POST['contactid'])) {
        $contactid = $_POST['contactid'];
    }

    if (isset($_GET['checksum'])) {
        $checksum = $_GET['checksum'];
    } elseif (isset($_POST['checksum'])) {
        $checksum = $_POST['checksum'];
    }

    if (isset($_GET['mailingid'])) {
        $checksum = $_GET['mailingid'];
    } elseif (isset($_POST['mailngid'])) {
        $checksum = $_POST['mailingid'];
    }

    # Facebook Pixel URL
    $fbURL = "https://www.facebook.com/tr?id=" . $facebookpixel . "&ev=PageView&noscript=1";

    # Google Maps URL
    $googleURL = "https://maps.googleapis.com/maps/api/js?key=" . $googleKey . "&libraries=places";

    #Google Analytics Key
    $googleAnalyticsURL = "https://www.googletagmanager.com/gtag/js?id=" . $googleAnalyticsKey;


    $urlpath = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $currentUrlpath = parse_url($_SERVER['REQUEST_URI']);
    $urlParameters = explode("&", $currentUrlpath["query"]);

    if ($keepurlparameters) {
        $successpage = $successpage . $currentUrlpath["query"];
    }
}
?>

<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Nutzen Sie unser neues FREE EMS® PROGRAMM und trainieren sie bequem von zu Hause aus.">
    <meta name="author" content="StimaWELL">
      
    <title>StimaWELL-EMS Training für nur 4,90 EURO</title>
      
    <link href="css/creative.css" rel="stylesheet" type="text/css">  
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">    
	<link rel="stylesheet" href="vendor/telefonvalidator-client/build/css/intlTelInput.css"/>
  </head>

    <script type="text/javascript" src="<?php echo $googleURL; ?>"></script>   	
	<script async src="<?php echo $googleAnalyticsURL;?>"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', '<?php echo $googleAnalyticsKey; ?>');
	</script>	
	<script>
	  /*Get the facebook pixel code from config.ini */
		var facebookpixel = <?=$facebookpixel ?>;
		!function(f,b,e,v,n,t,s)
		  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		  n.queue=[];t=b.createElement(e);t.async=!0;
		  t.src=v;s=b.getElementsByTagName(e)[0];
	      s.parentNode.insertBefore(t,s)}(window, document,'script',
		  'https://connect.facebook.net/en_US/fbevents.js');
		  fbq('init', <?=$facebookpixel ?> );
		  fbq('track', 'PageView'); 
	</script>
	<noscript>
		<img height="1" width="1" style="display:none" src= "<?php echo $fbURL ?>"/>
	</noscript>	
    
   <script> 
            // Set the date we're counting down to
        var countDownDate = new Date("<?= $config[GeneralSetting][CountdownExpire];?>").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

          // Get todays date and time
          var now = new Date().getTime();

          // Find the distance between now an the count down date
          var distance = countDownDate - now;

          // Time calculations for days, hours, minutes and seconds
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);

          // Display the result in the element with id="demo"
          document.getElementById("demo").innerHTML = days + " Tage " + hours + " Stunden "
          + minutes + " Min " + seconds + " Sek ";

          // If the count down is finished, write some text 
          if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "<?= $config[GeneralSetting][CountdownExpireMessage];?>";
          }
        }, 1000); 
</script>
    
  <body>

    <!-- DESKTOP NAV -->
    <nav class="navbar navbar-light bg-light static-top">   
      <div class="container">
          <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/" target="_blank"><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></a>
          <span class="navbar-details"style="text-transform:uppercase; "><i class="fas fa-check"></i> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;<i class="fas fa-check"></i> DIREKT VOM HERSTELLER&nbsp;&nbsp;<i class="fas fa-check"></i> SERVICE: <strong>+49 6443 83330</strong></span>
      </div>
    </nav>
      
    <!-- MOBILE HEADER-->  
    <div class="mobile-nav"><center><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></center></div>
      
      
    <!-- VIDEO HEADER -->
    <div class="video-header">
		<div class="video-overlay">
           	<center>
				<h1 style="color: #fff; font-size: 3rem; ">Noch nie war EMS Training so günstig und einfach.</h1>  
                <h2 style="color: #fff; font-size: 3rem; ">Nur <strong>4,90 Euro</strong> je EMS Training.</h2>  
				<p>&nbsp;</p>
				<button style="margin-bottom: 10px;" type="button" class="btn btn-outline-info" id="stimawell-button-1" data-toggle="modal" data-target="#modal-video">VIDEO ANSEHEN</button>
				<form style="display: inline;" action="#form"><button style="margin-bottom: 10px;" type="submit" class="btn btn-outline-info" id="stimawell-button-2">JETZT KAUFEN</button></form>
			</center>
		</div>
        
		<video id="video-background" preload="true" autoplay loop muted poster="img/bild_ems-training2.jpg">
			<source src="video/stimawell_anwenderfilm_1806.mp4" type="video/mp4">
		</video>
	</div>
	  
	<!-- MOBILE -->
    <div class="video-header-mobile">
		<div  class="video-overlay-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <p style="color: #fff; font-size: 2rem; ">Noch nie war EMS Training so günstig und einfach.</p>  
                        <p style="color: #fff; font-size: 1.2rem; ">Nur <strong>4,90 Euro</strong> je EMS Training.</p>  
                        <p>&nbsp;</p>
                        <button style="margin-bottom: 10px;" type="button" class="btn btn-outline-info"  data-toggle="modal" data-target="#modal-video">VIDEO ANSEHEN</button>
                        <form style="display: inline;" action="#form"><button style="margin-bottom: 10px;" type="submit" class="btn btn-outline-info">JETZT KAUFEN</button></form>
                    </div>    
                </div>
            </div>
		</div>
	</div>
      
      
    <!-- COUNTDOWN -->
    <section>
                 <div class="nav-bottom">
                   <div class="container">
                       <div class="row">
                         <div class="col-sm-12 text-center">

                             <span class="countdown-nav">Aktionsende:&nbsp;&nbsp;</span><span class="countdown-nav" id="demo"></span>&nbsp;<span class="countdown-nav">-&nbsp;&nbsp;Nur noch <?php echo $config[GeneralSetting][SoldOffers]; ?> von <?php echo $config[GeneralSetting][MaxOffers]; ?> Angeboten verfügbar.&nbsp;</span>
                             
                             <!--<form style="display: inline;" action="#form"><button style="margin-bottom: 10px;" type="submit" class="countdown-btn btn btn-outline-light">JETZT KAUFEN</button></form>-->

                          </div> 
                       </div>
                    </div>   
                </div>  

    </section>   
    <!-- ./COUNTDOWN -->      
    
    <p>&nbsp;</p>
      
	<section class="form-container">
    	<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-6" style="padding-top: 50px;">    
                    
					<h2 style="color: #000000;">FREE EMS - Trainieren Sie bequem<br>von zu Hause wann sie wollen</h2> 
					<hr>
					
					<ul>
                      <li>Trainiere für nur <strong>49 Euro im Monat</strong> (10 Trainingseinheiten für 49,00 Euro = 4,90 Euro je Trainingseinheit)</li>
                      <li>Bis zu <strong>80% günstiger</strong> als der Wettbewerb</li>
                      <li><strong>Kein versteckten Kosten</strong> - alles inklusive: Stimawell-EMS-Gerät, Anzug und Bandelektroden</li>
					  <li><strong>Kein Risiko</strong> - 6 Wochen Rückgaberecht nach Einweisung</li>
					  <li>Individuelles Trainingsprogramm</li>
                      <li><strong>Persönliche Betreuung</strong> durch erfahrene Trainer</li>
                      <li>EMS-Ausrüstung im Wert von <strong>8.299 Euro</strong> zur exklusiven Nutzung</li>  
                    </ul>
    
                    <div class="img-ghost">
            			<img style="width: 450px;" src="img/stimawell-model_2.png" alt="ems-training zu Hause"/>
                    </div>
                    
				</div>  
				<div class="col-lg-6 check-submit-form" id="form" style="padding-top: 50px; padding-bottom: 20px;">     
                    <form id="api-data-form" action="<?php echo $successpage?>" method="post" accept-charset="utf-8" class="form-horizontal" role="form">
                            <input type="hidden" name="contactid" value="<?= $contactid ?>">
                            <input type="hidden" name="checksum" value="<?= $checksum ?>">
                            <input type="hidden" name="mailingid" value="<?= $mailingid ?>">

                                <!-- INPUT ANREDE FRAU & HERR -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="btn-group">
											  <?php 
													
													if($_SESSION['data']['anrede'] === "Frau")
														 { $frau = "checked";}                    
													else { $herr = "checked";}
											  ?>										
                                              <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                                <input value="Frau" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne_Settings][Mapping][SALUTATION]; ?>" autocomplete="off" required<?php echo $frau; ?>> Frau
                                              </label>
                                                <span class="input-group-btn" style="width:15px;"></span> 
                                              <label class="btn btn-outline-secondary" style="border: 1px solid transparent;">
                                                <input value="Herr" style="width: 15px; height: 15px;" type="radio" name="<?php echo $config[MailInOne_Settings][Mapping][SALUTATION]; ?>" autocomplete="off" <?php echo $herr?>> 
												  Herr
                                              </label>
                                        </div>  
                                    </div>       
                                </div>

                                <!-- INPUT VORNAME -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
											
                                            <input value="<?php echo ((!empty($standard_001)? $standard_001:$_SESSION['data']['vorname']));?>" class="input-fields form-control" id="" name="<?php echo $vorname; ?>" placeholder="*Vorname" type="text" required 
											<? echo ($cutcopypaste) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>      
                                        </div>
                                    </div>       
                                </div>
                                
                                
                                <!-- INPUT NACHNAME -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
										
                                            <input  value="<?php echo ((!empty($standard_002)? $standard_002:$_SESSION['data']['nachname']));?>" class="input-fields form-control" id="nachname" name="<?php echo $nachname; ?>" placeholder="*Nachname" type="text" required
											<? echo ($cutcopypaste) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                        </div>
                                    </div>       
                                </div>
                                
                                <!-- INPUT ADRESSE -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                             <input value="<?php echo ((!empty($standard_004)? $standard_004:$_SESSION['data']['strasse']));?>"  class="input-fields form-control plz" id="strasse" name="<?php echo $adresse; ?>" placeholder="*Straße & Hausnummer" type="text" required 
											 <? echo ($cutcopypaste) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                        </div>
                                    </div>       
                                </div>

                                <!-- INPUT PLZ -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                             <input value="<?php echo ((!empty($standard_004)? $standard_004:$_SESSION['data']['plz']));?>"  class="input-fields form-control plz" id="plz" name="<?php echo $plz; ?>" placeholder="*PLZ" type="number" required 
											 <? echo ($cutcopypaste) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                        </div>
										<span class="p-light errorplz" style="color:<?php echo $plzerrcolor; ?>"><?php echo $plzerr; ?></span>     
                                    </div>       
                                </div>
                                
                                 <!-- INPUT ORT -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input value="<?php echo ((!empty($standard_005)? $standard_005:$_SESSION['data']['ort']));?>" class="input-fields form-control ort" id="ort" name="<?php echo $ort; ?>" placeholder="*Ort" type="text" required  
											<? echo ($cutcopypaste) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                        </div>
                                    </div>       
                                </div>

                                <!-- INPUT EMAIL -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input value="<?php echo ((!empty($email_002)? $email_002:$_SESSION['data']['email']));?>" class="input-fields form-control email" id="email" name="<?php echo $emailName; ?>" placeholder="*E-Mail-Adresse" type="email" required 
											<? echo ($cutcopypaste) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?>>
                                        </div>
                                        <?php if($_SESSION['error'] === "email"){ ?>   
                                             <span class="p-light erroremail"  style="color:<?php echo $config[Email_Address][ErrorMsg_Color]; ?>"><?php echo $config[Email_Address][ErrorMsg]; ?></span>
                                         <?php } ?>  										
                                    </div>       
                                </div>
                                
                                <!-- INPUT TELEFON -->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input value="<?php echo ((!empty($custom_003)? $custom_003:$_SESSION['data']['telefon']));?>" class="input-fields form-control" id="telefon-mobile" name="<?php echo $telefon; ?>" placeholder="*Telefonnummer" type="text" required <? echo ($cutcopypaste) ? 'onpaste="return false;" onCopy="return false;" onCut="return false;"':' ' ?> >
                                        </div>
										<span class="p-light errortelefon" style="color:<?php echo $telefonerrcolor; ?> ;float:right"><?php echo $telefonerr; ?></span>
                                    </div>       
                                </div>

                                <label class="form-group">
                                    <div class="col-sm-12">
                                    <input style="width: 20px; height: 20px;" type="checkbox" required>
                                        <span class="p-dark">&nbsp;&nbsp;Ich erkläre mich mit den <a target="_blank" href="https://www.stimawell-ems.de/agb">AGB</a>, <a target="_blank" href="https://www.stimawell-ems.de/de/mietbedingungen">Mietbedingungen </a>und <a target="_blank" href="https://www.stimawell-ems.de/datenschutz">Datenschutz </a>von Schwa-Medico Medizinische Apparate Vertriebsgesellschaft mbH einverstanden. Vertragslaufzeit beträgt 24 Monate.</span>
                                    </div>    
                                </label>    
                                
        

                                 <div class="form-group hidden">
                                  <label class="col-sm-2 control-label" for="">TRAFFICSOURCE</label>
                                  <div class="col-sm-10">
                                  <input class="form-control"  value="<?= $ts?>" placeholder="" type="text" name="<?php echo $ts; ?>" id="ts" value="<?php echo $constts; ?>">
                                  </div>
                                  </div>

                                  <div class="form-group hidden">
                                  <label class="col-sm-2 control-label" for="">Quelle</label>
                                  <div class="col-sm-10">
                                  <input class="form-control"  placeholder="" type="text" name="<?php echo $quelle; ?>" id="quelle" value="<?php echo $constquelle; ?>">
                                  </div>
                                  </div>

                                <div class="form-group hidden">
                                  <label class="col-sm-2 control-label" for="">Typ</label>
                                  <div class="col-sm-10">
                                  <input class="form-control"  placeholder="" type="text" name="<?php echo $typ; ?>" id="" value="<?php echo $consttyp; ?>">
                                  </div>
                                  </div>

                            <div class="form-group hidden">
                                  <label class="col-sm-2 control-label" for="">Segment</label>
                                  <div class="col-sm-10">
                                  <input class="form-control"  placeholder="" type="text" name="<?php echo $segment; ?>" id="" value="<?php echo $constsegment; ?>">
                                  </div>
                                  </div>

                           <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Url Parms</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="urlparms" id="" value="<?php echo htmlentities(serialize($urlParameters)); ?>">
                                </div>
                            </div>          
                                
                            <div class="form-group hidden">
                                <label class="col-sm-2 control-label" for="">Url Parms</label>
                                <div class="col-sm-10">
                                    <input class="form-control"  placeholder="" type="text" name="url" id="" value="<?php echo $urlpath; ?>">
                                </div>
                            </div>                                       

                            <div class="form-group"> 
                                <div class="col-sm-12">
                                <input style="margin-top: 25px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="JETZT KAUFEN" name="submit" style="width: 100%;">
                            </div>        
                            </div>
                            <div class="form-group"> 
                                <div class="col-sm-12">
                            <p>Hast du Fragen zum Angebot? Unsere Experten stehen für dich zur Verfügung. Rufe uns an unter <strong>+49 6443 83330</strong>.</p>
                              </div>        
                            </div>   
                           <?php if( isset($response) && $response->isSuccess() ) { ?>
                                <div class="alert alert-success fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription successful</strong>
                                </div>
                            <?php } elseif( isset($warning) ) { ?>
                                <div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong style="color: red; z-index: 1000;">Subscription failed</strong>
                                    <?= $warning['message'] ?>
                                </div>
                            <?php } elseif( isset($response) ) { ?>
                                <div class="alert alert-danger fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Subscription failed</strong>
                                </div>
                            <?php } ?>
                        </form>          
                        
                        
                    </div>
			</div>
        </div>  
    </section>
      
 
    <section class="info-content">
		<div class="container">
			<div class="row">
				<center>
					<div class="col-sm-12 col-md-10">
						<h2 style="color: #EEE">Was ist EMS-Training?</h2>
						<hr style="border-color: #EEE;">
						<h5 style="color: rgb(0, 162, 219);">Das innovative Training – mit minimalen Zeiteinsatz ein maximales Ergebnis erreichen</h5>
						<br>
						<p style="text-align: left; color: #EEE;">Dass elektrische Muskelstimulation eine hervorragende Methode in Puncto Schmerztherapie ist, die sich bereits in zahlreichen Kliniken fest etabliert hat, ist seit vielen Jahren bewiesen. Mit der Zeit wurde EMS auch als neue Ganzkörpertraining-Sportart immer bekannter. Die Muskeln werden durch kleine Stromimpulse trainiert. Mittlerweile zeigen jedoch zahlreiche Studien, dass mit EMS beste Ergebnisse erzielt werden können.</p>
						<p style="text-align: left; color: #EEE;">Egal ob zur Stärkung der Rückenmuskulatur, zum Muskelaufbau, zur Verminderung von Cellulite, zum Anregen des Stoffwechsels, zur nachhaltigen Figurformung oder auch einfach zum Entspannen – EMS ist besonders effektiv und zeitsparend!</p>
					</div>
				</center>
			</div>
		</div>  
    </section>
    
    <section class="testimonials">
		<div class="container">
          	<center>
          	<h2 style="color: #3c3b3b;">Das sagen unsere Kunden.</h2> 
           	<hr>
           	<p>Hier siehst du einige Erfahrungsberichte unserer Kunden und bei welchen Problemen ihnen FreeEMS von StimaWELL helfen konnte.</p>
            </center>
      	</div>
        
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
			</ol>
			<div class="carousel-inner">
			
				<div class="carousel-item active">
				  	<div class="container">
						<div class="row" style="padding-top: 50px; padding-bottom: 70px;">
							<div class="col-sm-12 col-md-3 text-center slider-bild-links">
								<img class="img-fluid" src="img/testimonial_zeitmangel.png" alt="testimonial zeitmangel">
							</div>
							<div class="col-sm-12 col-md-9 slider-text-rechts">
								<h5>Du hast keine Zeit für herkömmlichen Sport mit langen Trainings?</h5>
								<p>Im Alltag sammeln sich viele To-Dos an. Auch Anne H., Mutter von zwei Kindern, fällt es nicht leicht, sich Zeit für die eigene Gesundheit zu nehmen. Mit der FREE EMS-Heimanwendung, hat sich das jedoch geändert. <i>„Endlich kann ich trainieren wo und wann ich will. Mit EMS finde ich endlich wieder Zeit dazu, meinen Körper in Form zu bringen und meinen Beckenboden nach der Geburt zu stärken“</i>, erzählt Anne.<br>&nbsp;</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="carousel-item">
				  	<div class="container">
						<div class="row" style="padding-top: 50px; padding-bottom: 70px;">
							<div class="col-sm-12 col-md-3 text-center slider-bild-links">
								<img class="img-fluid" src="img/testimonial_figur.png" alt="testimonial figur">
							</div>
							<div class="col-sm-12 col-md-9 slider-text-rechts">
								<h5>Du hast Probleme mit Deiner Figur?</h5>
								<p>Viele Mütter haben Schwierigkeiten, ihre hartnäckigen Fettpölsterchen aus der Schwangerschaft loszuwerden – so auch Birgitt N. Doch mittlerweile hat die Mutter von drei Kindern das EMS-Training für sich entdeckt: <i>„16 cm weniger Bauchumfang und 12 cm weniger am Po. Endlich habe ich wieder Kleidergröße S und fühle mich wohl in meinem Körper!“</i> Birgitts Erfolgsrezept? Zwei EMS-Einheiten pro Woche, aufgeteilt in ein Kraft- und ein Cardiotraining – ohne zusätzlichen Sport und ohne Ernährungsumstellung.</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="carousel-item">
				  	<div class="container">
						<div class="row" style="padding-top: 50px; padding-bottom: 70px;">
							<div class="col-sm-12 col-md-3 text-center slider-bild-links">
								<img class="img-fluid" src="img/testimonial_ruecken.png" alt="testimonial rücken">
							</div>
							<div class="col-sm-12 col-md-9 slider-text-rechts">
								<h5>Du hast Probleme mit Deinem Rücken?</h5>
								<p>Rückenschmerzen sind die Volkskrankheit Nummer 1. Auch bei Claudia S., die an ihrem Arbeitsplatz lange Stehen muss, bahnten sich über die Jahre hinweg teilweise unerträgliche Rückenschmerzen an. Weder Massagen, noch Krankengymnastik oder Rückenschulen verhalfen Claudia zu einem langanhaltenden Erfolg. Erst als sie es mit einem Ganzkörper-EMS-Training versucht, verspürte sie eine erhebliche Verbesserung. <i>„Die Rückenschmerzen sind einfach verschwunden und auch seither nicht wieder aufgetaucht“</i>, erzählt Claudia.</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="carousel-item">
				  	<div class="container">
						<div class="row" style="padding-top: 50px; padding-bottom: 70px;">
							<div class="col-sm-12 col-md-3 text-center slider-bild-links">
								<img class="img-fluid" src="img/testimonial_relax.png" alt="testimonial relax">
							</div>
							<div class="col-sm-12 col-md-9 slider-text-rechts">
								<h5>Du suchst Entspannung & Ausgleich vom Alltag?</h5>
								<p>Bei den meisten Trainings kommt eines oft zu kurz: die Entspannung. Doch nicht bei EMS! Für Christine F. ist es wichtig, neben den normalen EMS-Trainingseinheiten auch die Regenerationsprogramme zu nutzen. <i>„Mindestens einmal pro Woche gönne ich mir das Vergnügen der Entspannungs- und Massageprogramme – so schalte ich vom Alltag ab und vergesse jeglichen Stress“</i>, erzählt Christine. Und das Beste daran: Dafür muss sie nicht einmal die Haustür verlassen.</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="carousel-item">
				  	<div class="container">
						<div class="row" style="padding-top: 50px; padding-bottom: 70px;">
							<div class="col-sm-12 col-md-3 text-center slider-bild-links">
								<img class="img-fluid" src="img/testimonial_cellulite.png" alt="testimonial cellulite">
							</div>
							<div class="col-sm-12 col-md-9 slider-text-rechts">
								<h5>Du hast Probleme mit Cellulite?</h5>
								<p><i>„Seit einigen Wochen trainiere ich nach dem speziellen EMS-Celluliteprogramm und mein Hautbild hat sich schon deutlich verbessert. Rechtzeitig zur Badesaison sind meine Oberschenkel und Hüfte endlich glatter“</i>, berichtet Dagmar K. Um Cellulite jedoch nachhaltig zu bekämpfen, reicht ein EMS-Training allein nicht aus. Denn gerade bei sogenannter „Orangenhaut“, spielt Ernährung eine wichtige Rolle. <i>„Daher habe ich mich auch an das EMS-Cellulite-Ernährungsprogramm gehalten – mit Erfolg“</i>, erzählt Dagmar.</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="carousel-item">
				  	<div class="container">
						<div class="row" style="padding-top: 50px; padding-bottom: 70px;">
							<div class="col-sm-12 col-md-3 text-center slider-bild-links">
								<img class="img-fluid" src="img/testimonial_muskeln.png" alt="testimonial muskeln">
							</div>
							<div class="col-sm-12 col-md-9 slider-text-rechts">
								<h5>Du möchtest Muskeln aufbauen?</h5>
								<p>EMS-Ganzkörpertraining kann Dich genauso wie Markus T. auf Hochtouren befördern. <i>„Seit Jahren bringt mich fast ausschließlich EMS-Muskeltraining in Form. Und mit dem Ergebnis bin ich sehr zufrieden“</i>, berichtet er. Markus trainiert zweimal die Woche für jeweils 20 Minuten. Diese Zeit reicht aus, um seine Muskeln zu definieren und perfekt in Form zu bringen. <i>„Die Leistungsfähigkeit und die Qualität der Muskel-Formung sind bei diesem Training unschlagbar“</i>, erzählt Markus.</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="carousel-item">
				  	<div class="container">
						<div class="row" style="padding-top: 50px; padding-bottom: 70px;">
							<div class="col-sm-12 col-md-3 text-center slider-bild-links">
								<img class="img-fluid" src="img/testimonial_beckenboden.png" alt="testimonial beckenboden">
							</div>
							<div class="col-sm-12 col-md-9 slider-text-rechts">
								<h5>Du suchst effektive Trainingsmethoden für den Beckenboden?</h5>
								<p>Grete S. lebte 40 Jahre lang mit einer Inkontinenz. Seitdem sie regelmäßig mit elektrischer Muskelstimulation trainiert, hat sich für sie einiges geändert: <i>„Ich fühle mich nicht mehr unsauber, kann meine Blase besser kontrollieren und kann endlich wieder ohne Beschwerden Treppen steigen! Das Training war das Beste, was mir je passiert ist und ich würde es jeder Frau empfehlen, die auch mit Inkontinenz zu kämpfen hat. Schade, dass es Ganzkörper-EMS nicht schon früher gab“</i>, erzählt Grete.</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="carousel-item">
				  	<div class="container">
						<div class="row" style="padding-top: 50px; padding-bottom: 70px;">
							<div class="col-sm-12 col-md-3 text-center slider-bild-links">
								<img class="img-fluid" src="img/testimonial_stoffwechsel.png" alt="testimonial stoffwechsel">
							</div>
							<div class="col-sm-12 col-md-9 slider-text-rechts">
								<h5>Du hast Stoffwechselprobleme?</h5>
								<p>Als Bärbel P. ihrer Nikotinsucht den Kampf ansagte, nahm sie einiges an Gewicht zu. Jahrelang versuchte Bärbel alles, um ihre Extrakilos loszuwerden – leider ohne Erfolg. Doch mit EMS-Training fand sie endlich eine einfache Lösung für ihr Problem: <i>„Ich habe das EMS-Stoffwechselprogramm und die Stoffwechselkur gleich zweimal mit großem Erfolg durchgeführt. Und ich habe tatsächlich in nur wenigen Monaten 12 kg abgenommen – sogar ohne weiteren Sport!“</i>, erzählt Bärbel.</p>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		  
			  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			  </a>
			  
		</div>
		 
		 
	  	<div class="container">
	  		<div class="row">
				<div class="col-lg-12">
                	<center>
					<a href="#form"><input style="margin-top: 25px; margin-bottom: 25px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-8 btn btn-danger btn-xl js-scroll-trigger" value="JETZT KAUFEN" style="width: 100%;"></a>
                	</center>   
				</div>
			</div>
	  	</div>
    </section>  

    <section class="partner-container">
    <div class="container">
    <div class="row">
         <div class="col-sm-6 model-feature">       
            <h2>Weil Fitness unkompliziert und bezahlbar sein soll.</h2>
            <hr>
            <p class="lead mb-0">Erlebe die neue, freie Art des EMS-Trainings. Erreiche deine Trainingsziele schneller und bequemer. Kombiniere die regelmäßige, persönliche Betreuung durch deinen EMS-Experten und dein eigenständiges EMS-Training mit dem virtuellen Trainer zu Hause oder unterwegs.</p><br>
            
        </div>  
                <div class="col-sm-6 text-center">
                    <img style="width: 350px; margin-top: 25px;" class="hidden-model" src="img/stimawell-model_1.png" alt="ems-training zu Hause"/>
                </div>
            </div>
        </div>  
    </section>  
       
    <section class="partner-container" style="background-image: url('img/ems-bg.jpg');">
    <div class="container">
    <div class="row">
                <div class="col-sm-6 text-center">
                    <img style="width: 350px; margin-top: 25px;" class="hidden-model" src="img/stimawell-model_3.png" alt="ems-training zu Hause"/>
                </div>
				<div  class="col-sm-6 model-feature">       
					  <h2>Vielseitige Anwendung um deine Ziele schneller zu erreichen.</h2>
					  <hr>
					  <p class="lead mb-0">Die EMS Technologie ermöglicht es dir Ziele verschiedenster Art schneller zu erreichen. Die Kombination aus persönlicher Betreuung durch deinen EMS-Experten und eigenständigem EMS-Training machen es möglich. Wo und wann du willst!</p><br>
                   
				</div>  
            </div>
        </div>  
    </section>  
      
    <section class="partner-container">
    <div class="container">
    <div class="row">
        <div class="col-sm-6 text-center">
            <img class="partner-img" style="width: 400px; padding-top:70px;" src="img/stimawell-dominic-heilig-mobile.jpg" alt="ems-experte philipp akgüzel zu Hause"/>
            <center><img class="img-fluid partner-img-mobile" src="img/stimawell-dominic-heilig-mobile.jpg" alt="ems-experte philipp akgüzel zu Hause"></center>
         </div>
        <div style="padding: 50px 0px 50px 0px; " class="col-sm-6">       
      
            <h2>Dein persönlicher Ansprechpartner und EMS-Experte für dein Training zu Hause ist <span style="color:#00a2db">Dominic Heilig</span>.</h2>
            <hr>
			<p>Mein Name ist Dominic Heilig. Seit meinem 5. Lebensjahr treibe ich leidenschaftlich gerne Sport. Aus dieser Leidenschaft heraus resultiert mein Beruf. Während meines Studiums zum Bachelor of Arts in Fitnessökonomie durchlief ich mehrere Stationen. Durch die Vielzahl an Stationen konnte ich ein umfassendes Fachwissen aufbauen, was ich gerne heute an meine Kunden und Partner weitergebe.</p>
			<p>Durch meine Arbeit in zwei Microstudios habe ich täglich im Umgang mit den Kunden gelernt, welche Herausforderungen zu lösen sind, welche Ziele die Kunden haben und vor allem wie man diese Ziele am besten erreicht. Seit 2017 betreute ich für StimaWELL unsere Kunden für die Ganzkörper EMS Systeme. Meine Arbeit bringt mich mit vielen verschiedenen Kunden zusammen. Ich helfe sowohl Privatkunden als auch professionellen Anwendern wie Physiotherapeuten, Fitnessstudios, Sonnenstudios, Hebammen, Personal-Trainern, Heilpraktikern oder EMS-Studios. Mit meiner mehrjährigen Erfahrung im EMS-Training Bereich berate ich sehr gerne Existenzgründer und helfe diesen mit innovativen EMS Produkten von schwa-medico erfolgreich eine Existenz aufzubauen.</p>
             <a href="#form"><input style="margin-top: 25px; margin-bottom: 25px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="submit" class="col-sm-12 btn btn-danger btn-xl js-scroll-trigger" value="JETZT KAUFEN" style="width: 100%;"></a>
            
        </div>         
    </div>
    </div>  
    </section>  
      
    <!-- MODAL -->
	<div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="modal-video-label">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content" style="background-color: #1A1A1A;">
				<div class="modal-header" style="border-bottom: none; padding-top: 9px;">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" style="color: #FFF;">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="padding: 0px 0px 40px 0px;">
       				<div class="modal-video">
						<center>
                        	<video id="video-modal" controls controlsList="nodownload" preload="none" poster="img/bild_ems-training2.jpg">
								<source src="video/stimawell_anwenderfilm_klein.mp4" type="video/mp4">
							</video>
                            <button onclick="javascript:window.location='https://stimawell.campaign-in-one.de/aktion/index.php#form'" data-dismiss="modal" style="margin-top: 15px; cursor: pointer; background-color: rgb(0, 162, 219); border-color: rgb(0, 162, 219);" type="button" class="col-sm-8 btn btn-danger">JETZT KAUFEN</button>
                    	</center> 
                	</div>
               	</div>
          	</div> 
      	</div>
    </div>
    <!-- ./MODAL -->
      
    <section>
        <div class="clearspace"></div>
    </section>  
      
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 h-100 text-center text-lg-left my-auto" style="height:auto!important">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/de/mietbedingungen" target="_blank">Mietbedingungen</a>
              </li>
            </ul>
            <p style="padding-top:15px;" class="text-muted small mb-4 mb-lg-0"><strong>✔</strong> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;&nbsp;<strong>✔</strong> DIREKT VOM HERSTELLER&nbsp;&nbsp;&nbsp;<strong>✔</strong> SERVICE: 06443 4369914</p>
          </div>
          <div class="col-lg-4 h-100 text-center text-lg-right my-auto" style="height:auto!important">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                  <i class="fa fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                  <i class="fa fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
            
        </div>   
      </div>
    </footer>
      

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="vendor/telefonvalidator-client/build/js/utils.js"></script>		
	<script type="text/javascript" src="vendor/telefonvalidator-client/build/js/intlTelInput.js"></script>	
    <script type="text/javascript" src="jquery.js"></script>    
    <script src="vendor/google/maps.js"></script>  
        
            
          <script>
    
            $(window).scroll(function() {    
            var scroll = $(window).scrollTop();

            if (scroll > 660) {
                $(".nav-bottom").addClass("change"); // you don't need to add a "." in before your class name
            } else {
                $(".nav-bottom").removeClass("change");
            }
        });
        </script>
          
            
            
            
            
 
  </body>
</html>