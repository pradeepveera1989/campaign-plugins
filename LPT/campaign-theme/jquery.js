

        
        $(document ).ready(function() {
            var autofill_postal_code = "<?= $plzstatus;?>";
            console.log("autofill_postal_code", autofill_postal_code);    
			var validate_telefon = "<?= $telefonstatus;?>";
			
			// telefon validation 
			var settings = {
				utilsScript: "vendor/telefonvalidator-php-client/build/js/utils.js",
				preferredCountries: ['de'],			
				onlyCountries:['de','ch','at'],
			};		
			$('#telefon').intlTelInput(settings);
			$('#telefon-mobile').intlTelInput(settings);				

			// Error messages
            $('.errorplz').hide();
			$('.errortelefon').hide();
            
            //Parse config.ini file 
                        
            
            /* JQuery Blur event for the postal code
             * Parameters : 
             * Returns : 
             *    1. fill the city Name on success.
             */
            
            $('.plz').blur(function(){
                var zip = $(this).val();
                var city;
               
                
                if($('.plz').hasClass('wrongplz')){
                    $('.plz').removeClass('wrongplz'); 
                    $('.errorplz').hide();
                }
                // Check for autofill postal code value
                if(autofill_postal_code){      
                    city = checkZipCode(zip);
                    
                    if(city){
                        $('.ort').val(city);
                    }else{
                        $('.ort').val("");      
                        $('.plz').addClass("wrongplz");
                    }
                }    
            });                


            /* Function Name : checkZipCode
             * Parameters : zipcode
             * Returns : 
             *    1. countryName on success .
             *    2. False on faiure.
             */
            
            function checkZipCode(zip){
                var status = true;
                var city ;
                if(zip.length == 5) {
                    var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=Germany'+zip+'&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I';
                    $.getJSON({
                        url : gurl,
                        async: false,
                        success:function(response, textStatus){
                            // check the status of the request
                            if(response.status !== "OK") {
                                status = false;         // Postal code not found or wrong postal code.
                            } else{    
                                // Postal code is found
                                status = true
                                var address_components = response.results[0].address_components;
                                $.each(address_components, function(index, component){
                                    var types = component.types;
                                    // Find the city for the postal code
                                    $.each(types, function(index, type){
                                        if(type == 'locality'){
                                            city = component.long_name;
                                            status = true;
                                        }
                                    });
                                });                                 
                            }
                         }
                    });
                } else{        
                    status = false;
                }
                if(status){
                    return city;
                }else {
                    return false;
                }
            }

			/* Desktop Version
			*  Validates the phone number format for specified country. 
			*
			*  Parameters:  Telephone field in the form.
			*
			*  Returns: 
			*		false : shows the error message 
			*       true  : hides the error message
			*/  	
			
			$('#telefon').blur(function() {
				/* Regex for all the special characters and alphabits */
				alpha = /^[a-zA-Z!@#$§?=´:;<>|{}\[\]*~_%&`.,"]*$/;
				tel = $('#telefon'); 
				var error = false;
				telnum = tel.val();
				
				// If telefon validation true 
				if ($.trim(tel.val()) && validate_telefon) {
					console.log(telnum);
				// Check for alphabits and special characters from regex expresssion.
					for(var i = 0;  i < telnum.length; i++){
						var c = telnum.charAt(i);
						if(alpha.test(c)){
							error = true;
						}
					}
					console.log(error);
				// Check for valid number  
					if (!tel.intlTelInput("isValidNumber")) {
						
						$('.errortelefon').show();
					}else {
						if(error){
							$('.errortelefon').show();
						}else{
							$('.errortelefon').hide();
						}	
					}					
				}
			});	
			
			/* Mobile version
			*  Validates the phone number format for specified country. 
			*
			*  Parameters:  Telephone field in the form.
			*
			*  Returns: 
			*		false : shows the error message 
			*       true  : hides the error message
			*/  			
			
			$('#telefon-mobile').blur(function() {
				/* Regex for all the special characters and alphabits */
				alpha = /^[a-zA-Z!@#$§?=´:;<>|{}\[\]*~_%&`.,"]*$/;
				tel = $('#telefon-mobile'); 
				telnum = tel.val();				
				var error = false;
				// If telefon validation true 
				if ($.trim(tel.val()) && validate_telefon) {
				// Check for alphabits and special characters from regex expresssion
					for(var i = 0;  i < telnum.length; i++){
						var c = telnum.charAt(i);
						if(alpha.test(c)){
							error = true;
						}
					}			
				// Check for valid number  
					if (!tel.intlTelInput("isValidNumber")) {
						$('.errortelefon').show();
					}else {
						if(error){
							$('.errortelefon').show();
						}else{
							$('.errortelefon').hide();
						}	
					}					
				}
			});						
			
            /* JQuery submit event for the postal code
             * Parameters : 
             * Returns : 
             *    1. check the postal code and accordingly display the error message for postal code.
             */
            form = $('.check-submit-form');
            form.submit(function(e)  {
               //e.preventDefault();
                var form = $(this);
                var readyforsubmit = true;     
                var plz = $('.plz');
                var ort = $('.ort');
                var email = $('.email');
                
                if(autofill_postal_code) {
                    if(plz.hasClass("wrongplz")){    
                        $('.errorplz').show();
                        return false;
                    }
                }
				if(validate_telefon) {
					if($('.errortelefon').css('display') !== 'none'){
						return false;
					}
				}				
				form.find("input[type='submit']").prop('disabled', true);
            }); 
        });     