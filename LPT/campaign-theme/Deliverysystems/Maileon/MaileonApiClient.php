<?php

/**
 * Description of MaileonApiClient
 *
 * @author Pradeep
 */
include ('/is/htdocs/wp13026946_P396R5TO8O/www/online-mehr-geschaeft.de/treaction/LPT/config.php');
require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/maileon-php-client-1.3.1/client/MaileonApiClient.php');

function getMaileonConstantFeilds(){
    include ('/is/htdocs/wp13026946_P396R5TO8O/www/online-mehr-geschaeft.de/treaction/LPT/config.php');

    $field = array();

    if(!empty($maileonconstant)){
        foreach ($maileonconstant as $key => $value) {
            array_push($field, $key);
        }        
    }
    return $field;
}

function getMaileonConstantFeildValues(){
    include ('/is/htdocs/wp13026946_P396R5TO8O/www/online-mehr-geschaeft.de/treaction/LPT/config.php');
    $fieldvalue = array();
    if(!empty($maileonconstant)){
        foreach ($maileonconstant as $key => $value) {
            array_push($fieldvalue, $value);
        }        
    }
    return $fieldvalue;
}

function createMaileonContact($contactfields, $resultCustomFields) {
    
    include ('/is/htdocs/wp13026946_P396R5TO8O/www/online-mehr-geschaeft.de/treaction/LPT/config.php');
    require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/maileon-php-client-1.3.1/client/MaileonApiClient.php');
    
    $constantfields = array();
    $constandfieldvalues = array();
    $urlparms = $contactfields[urlparms];
    
    $constantfields = getMaileonConstantFeilds();
    $constantfieldvalues = getMaileonConstantFeildValues();

    $countoffields = sizeof($constantfields);
    
    $newContact = new com_maileon_api_contacts_Contact();
    $newContact->email = $contactfields[$emailName];
    $newContact->anonymous = false;
    $newContact->permission = com_maileon_api_contacts_Permission::$DOI_PLUS;
    $newContact->standard_fields["SALUTATION"] = $contactfields[$anrede];
    $newContact->standard_fields["FIRSTNAME"] = $contactfields[$vorname];
    $newContact->standard_fields["LASTNAME"] = $contactfields[$nachname];
    $newContact->standard_fields["ADDRESS"] = $contactfields[$adresse];
    $newContact->standard_fields["ZIP"] = $contactfields[$plz];
    $newContact->standard_fields["CITY"] = $contactfields[$ort];
    $newContact->custom_fields["Vertriebsmitarbeiter"] = $vertriebsmitarbeiter;
    $newContact->custom_fields["Telefon"] = $contactfields[$telefon];
    $newContact->custom_fields["ip_adresse"] = $contactfields[$ip];
    $newContact->custom_fields["url"] = $contactfields[$url];
    $newContact->custom_fields["HubspotContactID"] = $contactfields[$hubspotcontact];
    $newContact->custom_fields["HubspotDealID"] = $contactfields[$hubspotdeal];

    while ($countoffields > 0) {
        if (isset($constantfields[$countoffields])) {
            $newContact->custom_fields[$constantfields[$countoffields]] = $constantfieldvalues[$countoffields];
        }
        $countoffields = $countoffields - 1;
    }

    #Get the URL parameters
    foreach ($urlparms as $parm) {
        var_dump("parm",$parm);
        $leftParm = explode("=", $parm)[0];
        var_dump("leftparm", $leftParm);
        # Search URL Parameters in Maileon custom fields
        if (array_key_exists($leftParm, $resultCustomFields)) {
            if (array_search($leftParm, $config[MailInOne_Settings][Mapping])) {
                $customField = array_search($leftParm, $config[MailInOne_Settings][Mapping]);
                #Update the new contact field with value.
                $newContact->custom_fields[$customField] = explode("=", $parm)[1];
            }
        }
    }

    return $newContact;
} 
