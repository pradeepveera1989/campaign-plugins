<?php

/*
 * 
 * Description of HubspotApiClient
 *
 * @author Pradeep
 */

// Update with Hubspot 
require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/hubspot-php-client/class.contacts.php');
require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/hubspot-php-client/class.company.php');
require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/hubspot-php-client/class.deals.php');

$config = parse_ini_file('config.ini', true);


/*
 * 
 * 
 */

function getHubspotContactParms($parms){
    // Contact HubSpot
    include ('/is/htdocs/wp13026946_P396R5TO8O/www/online-mehr-geschaeft.de/treaction/LPT/config.php');
    
    $contactparmsHSpot = array(
        'salutation' => $parms[$anrede],
        'firstname' => $parms[$vorname],
        'lastname' => $parms[$nachname],
        'zip' => $parms[$plz],
        'city' => $parms[$ort],
        'email' => $parms[$emailName],
        'phone' => $parms[$telefon],
        "hubspot_owner_id" => $parms[$vertriebid],
    );
    
    return $contactparmsHSpot;
}

/*
 * 
 * 
 *  
 */

function getHubspotContactId($postfields,  $apikey, $updaterecord) {

    $contactHSpot = new HubSpot_Contacts($apikey);

    $email = $postfields[email];
    $contactparmsHSpot = getHubspotContactParms($postfields);
    $contactByEmailHSpot = $contactHSpot->get_contact_by_email($email);

    if (!is_null($contactByEmailHSpot->vid)) {
        $contactIdHSpot = $contactByEmailHSpot->vid;
        #Parameter to update the record
        if ($updaterecord) {
            $updatedContactHSpot = $contactHSpot->update_contact($contactIdHSpot, $contactparmsHSpot);
            if (is_null($updatedContactHSpot->vid)) {
                error_log("\n Error:Unable to update the contact details", 3, "error.log");
            }
        }
    } else {
        // Create a new contact on Hubspot
        $contactIdHSpot = $contactHSpot->create_contact($contactparmsHSpot);
        $contactIdHSpot = $contactIdHSpot->vid;
        if (is_null($contactIdHSpot)) {
            error_log("\n Error:Unable to create a contact details", 3, "error.log");
        }
    }
    
    return $contactIdHSpot;
}


/*
 * 
 * 
 *
 * 
 */

function getHubspotCompanyId($hubspotcomp, $apikey, $email) {

    $companyHSpot = new HubSpot_Company($apikey);
    $emailDomain = substr(strrchr($email, "@"), 1);

    $companyparmHSpot = array(
        'name' => $hubspotcomp[name],
        'domain' => $hubspotcomp[domain],
        'description' => $hubspotcomp[description],
        'phone' => $hubspotcomp[phone],
        'zip' => $hubspotcomp[zip],
    );

    $companyparmSearch = array(
        'limit' => 15,
        'requestOptions' => array(
            'properties' => array(
                0 => 'domain',
                1 => 'createdate',
                1 => 'name',
                2 => 'hs_lastmodifieddate',
            ),
        ),
        'offset' => array(
            'isPrimary' => true,
            'companyId' => 0,
        ),
    );

    // HubSpot contact check by email.
    $searchByDomainHSpot = $companyHSpot->get_company_by_domain($emailDomain, $companyparmSearch);

    $countDomains = sizeof($searchByDomainHSpot->results);

    // Company does not exist
    if ($countDomains == 0) {

        if (isset($hubspotcomp[domain])) {
            $companyIdHSpot = $companyHSpot->create_company($companyparmHSpot);
            $companyIdHSpot = $companyIdHSpot->companyId;
            if (is_null($companyIdHSpot)) {
                error_log("Error: Unable to create company", 3, "error.log");
            }
        } else {
            // Dont create a new company
            $companyIdHSpot = 0;
        }
    } else {
        // Update the new company 
        foreach ($searchByDomainHSpot->results as $domain) {
            $companyIdHSpot = $domain->companyId;
        }
    }

    return $companyIdHSpot;
}

/*
 * 
 *
 *
 */

function getHubspotDealParms($parms) {

    include ('/is/htdocs/wp13026946_P396R5TO8O/www/online-mehr-geschaeft.de/treaction/LPT/config.php');    
    $dealname = $hubspotdealname . " " . $parms[$vorname] . " " . $parms[$nachname] . " " . $parms[$ort] . " " . date("Y-m-d");

    $dealparms = array(
        "dealname" => $dealname,
        "amount" => $hubspotdealamount,
        "segment" => $hubspotdealsegement,
        "dealstage" => $hubspotdealstage,
        "pipeline" => $hubspotdealpipeline,
        "qualitaet" => $hubspotdealqualitaet,
        "hubspot_owner_id" => $parms[$vertriebid],
    );
 
    return $dealparms;
}

/*
 * 
 * 
 * 
 */
function getHubspotDealAss($parms) {

    include ('/is/htdocs/wp13026946_P396R5TO8O/www/online-mehr-geschaeft.de/treaction/LPT/config.php');
    $contactid = $parms[$hubspotcontact];
	$dealasociation = array(
        "associations" => array(
            "associatedVids" => array(
                0 => $contactid
            ),
        ),
    );
    return $dealasociation;
}

/*
 * 
 * 
 * 
 * 
 */
function getHubspotDealId($postfields, $apikey) {

    $dealHSpot = new HubSpot_Deal($apikey);
    $dealCreated = false;
    $dealparms = getHubspotDealParms($postfields);
    #var_dump($postfields);
    $dealassociation = getHubspotDealAss($postfields);
    $recentDeals = $dealHSpot->get_recent_deals()->results;
    foreach ($recentDeals as $deals) {
        if ($deals->properties->dealname->value === $dealparms[dealname]) {
            $dealCreated = true;
            $dealId = $deals->dealId;
        }
    }

    if (empty($recentDeals) || !$dealCreated || empty($dealId)) {
        // Associate contact and company with new Deal
        $dcc = $dealHSpot->create_deal($dealassociation, $dealparms);
        $dcc = json_decode($dcc);
        $dealId = $dcc->dealId;
        $hubspotdealid = $dealId;
    }
    return $dealId;
}

/*
 * 
 * 
 * 
 * 
 * 
 */

function associateContactAndDeal($apikey, $contactId, $companyId, $dealId){
    $dealHSpot = new HubSpot_Deal($apikey);
    
    if(!is_null($dealId && $companyId)){
        $dccontact = $dealHSpot->associate_deal_with_contact($dealId, $contactId);
    }
    if(!is_null($dealId && $companyId)){
        $dccompany = $dealHSpot->associate_deal_with_company($dealId, $companyId);
    }    
}
