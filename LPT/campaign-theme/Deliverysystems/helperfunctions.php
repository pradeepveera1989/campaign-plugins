<?php

/*
  1. check the email address and accordingly

 */
$config = parse_ini_file("config.ini", true);

require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/emailvalidation-php-client/emailchecker.php');

#Database
$database = $config[Database][DbName];
$user = $config[Database][User];
$dbname = $config[Database][DbName];
$password = $config[Database][Password];
$host = $config[Database][Host];

function validateEmailAddress($email) {
    if (isset($email)) {
        $emailchecker = new emailChecker($email);
        $emailchecker->curlRequest();
        $emailchecker->curlResponce();
        $resp = $emailchecker->parseResponce();
        return $resp;
    }
}

function getAssignmentManager($postalCode, $db) {
    
    #Connection to the database
    $verbindung = new mysqli($db[Host], $db[User], $db[Password], $db[DbName]) or die("Keine Verbindung");
    
    #SQL Query to retrive the vertriebsmitarbeiter according to postalcode.
    $sqlQuery = "SELECT `AssigneValue` FROM `AssignmentManager` WHERE `PostalCodeFrom` <= '" . $postalCode . "' AND `PostalCodeTo` >= '" . $postalCode . "' LIMIT 0, 30 ";
    $result = $verbindung->query($sqlQuery);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            #Found the value
            $vertriebsmitarbeiter = $row["AssigneValue"];
        }
    } else {
        # Default value for Vertriebsmitarbeiter
        $vertriebsmitarbeiter = $config[AssignmentManager][Vertriebsmitarbeiter];
    }
    
    return $vertriebsmitarbeiter;
}



function getAssignmentManagerId($vertriebsmitarbeiter, $db){
    
    if (isset($vertriebsmitarbeiter)) {
    
        #Connection to the database
        $verbindung = new mysqli($db[Host], $db[User], $db[Password], $db[DbName]) or die("Keine Verbindung");        
        
        $queryGetHubspotId = "SELECT `HubspotId` FROM `AssignmentManagerInfo` WHERE `AssignmentManager` LIKE '" . $vertriebsmitarbeiter . "'";

        $result = $verbindung->query($queryGetHubspotId);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $vertriebsmitarbeiterid = $row["HubspotId"];
            }
        }
        return $vertriebsmitarbeiterid;
    }   
}