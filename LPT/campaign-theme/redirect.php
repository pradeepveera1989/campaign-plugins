<?php

// Get the config data from config.ini       
session_start();
include('config.php');
require_once('Deliverysystems/AssignmentManager.php');
require_once('Deliverysystems/Hubspot/HubspotApiClient.php');
require_once('Deliverysystems/Maileon/MaileonApiClient.php');
require_once('Deliverysystems/helperfunctions.php');
require_once('/is/htdocs/wp13026946_P396R5TO8O/www/campaign-in-one.de/php-client/maileon-php-client-1.3.1/client/MaileonApiClient.php');


$config = parse_ini_file("config.ini", true);
$postfields = array();

if (isset($_POST[$emailName])) {
    #Validate the Email address
    $email = $_POST[$emailName];
    $emailcheck = validateEmailAddress($email);
    if (!$emailcheck) {
        // invalid email address
        // Copying the data to session and intiating a sesssion variable error.
        $_SESSION['data'] = $_POST;
        $_SESSION['error'] = "email";
        $homeurl = $_POST[$url].'#form';
        header('Location: '. $homeurl); 
        exit();
        
    } else {

        $_SESSION['error'] = "";
        $postfields = $_POST;
        $_SESSION['data'] = array();
        $urlparms = unserialize($_POST["urlparms"]);

        if ($keepurlparameters) {
            $successpage .= implode("&", $urlparms);
        }

        $ip = $_SERVER['REMOTE_ADDR'];

        $postfields[ip] = $ip;

        // Probetraining only for Welcome leads and Adwords.
        $probetraining = $config[MailInOne_Settings][Probetraining];
        $trafficsource = $_POST["ts"];
        
        #Assigning the Sales Manager depending on Postalcode
        if (isset($postfields[$plz]) && $assignmentstatus) {

            $postalCode = $postfields[$plz];
            $assignmentmanager = new AssignmentManager($config[Database]);
            $vertriebsmitarbeiter = $assignmentmanager->getAssignmentManager($postfields[$plz]);
            $vertriebsmitarbeiterid = $assignmentmanager->getAssignmentManagerId($vertriebsmitarbeiter);
            $postfields[$vertrieb] = $vertriebsmitarbeiter;
            $postfields[$vertriebid] = $vertriebsmitarbeiterid;
        
        }  

        //Hubspot Configuration Sync            
        if ($hubspotsync) {
                
            $hubspotcontactid = getHubspotContactId($postfields, $hubspotkey, $updateexistingrecord);
            $postfields[$hubspotcontact] = $hubspotcontactid;           
            $hubspotcompanyid = getHubspotCompanyId($config[HubSpotCompany], $hubspotkey, $contactparmsHSpot[email]);         
            $hubspotdealid = getHubspotDealId($postfields, $hubspotkey);
            $postfields[$hubspotdeal] = $hubspotdealid;
            associateContactAndDeal($hubspotkey, $hubspotcontactid, $hubspotcompanyid, $hubspotdealid);

        }

        if($maileonstatus){
           $contactsService = new com_maileon_api_contacts_ContactsService($maileonconfig);
           $contactsService->setDebug(false);	

           # Get all the custom fields from the Maileon.
           $getCustomFields = $contactsService->getCustomFields();
           $resultCustomFields = $getCustomFields->getResult()->custom_fields;
           $resultCustomFields = array_change_key_case($resultCustomFields, CASE_LOWER);       
           
           $getContact = $contactsService->getContactByEmail($email);
           if($getContact->isSuccess()){
               
               #Update the existing contact.
               $contact = createMaileonContact($postfields, $resultCustomFields);
           }else{
               
               #create new contact.
               $contact = createMaileonContact($postfields, $resultCustomFields);
           }
           $response = $contactsService->createContact($contact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, '', '', false, false, $maileondoikey);
        }
    }
}  
?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">  
	<?php if($config[GeneralSetting][Redirect][RedirectStatus]){ ?>  
    <meta http-equiv="refresh" content="<?php echo $redirecttime; ?>;url=<?php echo $redirectpath; ?>" />  
	<?php } ?>
    <title>EMS Training zu Hause</title>
    <!-- implementation bootstrap -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- implementation fontawesome icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
       <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- implementation simpleline icons -->
    <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <!-- implementation googlefonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <!-- implementation Animated Header -->
    <!-- implementation custom css -->
    <link href="css/creative.css" rel="stylesheet">
    <!-- implementation animate css -->
    <link href="css/animate.css" rel="stylesheet">
  </head>
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '2069635113314986');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=2069635113314986&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->
  <body>

    <!-- DESKTOP NAV -->
    <nav class="navbar navbar-light bg-light static-top">   
      <div class="container">
          <a class="navbar-brand" style="text-transform: uppercase;" href="https://www.stimawell-ems.de/" target="_blank"><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></a>
          <span style="text-transform:uppercase; "><i class="fas fa-check"></i> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;<i class="fas fa-check"></i> DIREKT VOM HERSTELLER&nbsp;&nbsp;<i class="fas fa-check"></i> SERVICE: <strong>+49 6443 83330</strong></span>
      </div>
    </nav>
      
    <!-- MOBILE HEADER-->  
    <div class="mobile-nav"><center><img style="width:200px;" alt="EMS-Training-zuhause" src="img/logo.svg"></center></div>

    <!-- FORM -->
    <section class="form-container" style="padding-top: 250px; padding-bottom: 250px; height:100%;" id="about">
    <div class="container">  
    <div class="row">
    <div class="col-sm-12">
        <center>
            
            <h1 style="color:; text-transform: uppercase;">Bitte überprüfe dein E-Mail Postfach.</h1>
            <hr>
            <p style="color:;">Wir haben dir eine E-Mail mit einem Bestätigungslink an deine genannte E-Mail-Adresse gesendet. Solltest du in den nächsten 15 Minuten keine Mail erhalten, schaue bitte auch in dein Spam oder Junk Ordner.</p> 
        </center>
    </div>
    </div>
    </div>  

</section>  
 <div class="clearspace"></div>     
 <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 h-100 text-center text-lg-left my-auto" style="height:auto!important">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/impressum" target="_blank">Impressum</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/agb" target="_blank">AGB</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/datenschutz" target="_blank">Datenschutz</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="https://www.stimawell-ems.de/de/mietbedingungen" target="_blank">Mietbedingungen</a>
              </li>
            </ul>
            <p style="padding-top:15px;" class="text-muted small mb-4 mb-lg-0"><strong>✔</strong> 40 JAHRE MADE IN GERMANY&nbsp;&nbsp;&nbsp;<strong>✔</strong> DIREKT VOM HERSTELLER&nbsp;&nbsp;&nbsp;<strong>✔</strong> SERVICE: 06443 4369914</p>
          </div>
          <div class="col-lg-4 h-100 text-center text-lg-right my-auto" style="height:auto!important">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="https://www.facebook.com/stimawellems/" target="_blank">
                  <i class="fa fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="https://www.instagram.com/stimawell.ems/" target="_blank">
                  <i class="fa fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
            
        </div>   
      </div>
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js">
    </script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  
    <script src="vendor/header-animation/TweenLite.min.js">
    </script>
    <script src="vendor/header-animation/EasePack.min.js">
    </script>
    <script src="vendor/header-animation/rAF.js">
    </script>
    <script src="vendor/header-animation/demo-1.js">
    </script>  

  </body>

</html>