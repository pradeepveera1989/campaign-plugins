<?php
/**
* Copyright 2013 HubSpot, Inc.
*
*   Licensed under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied.  See the License for the specific
* language governing permissions and limitations under the
* License.
*/
require_once('class.baseclient.php');

class HubSpot_Deal extends HubSpot_BaseClient{
	//Client for HubSpot Contacts API

	    //Define required client variables
	protected $API_PATH = 'deals';
	protected $API_VERSION = 'v1';


    /**
    * Create a Deal
    *
    *@param params: array of properties and property values for new contact, email is required
    *
    * @return Response body with JSON object 
    * for created Contact from HTTP POST request
    *
    * @throws HubSpot_Exception
    **/
    public function create_deal($associations, $params){
    	$endpoint = 'deal';
    	$properties = array();
     
    	foreach ($params as $key => $value) {
    		array_push($properties, array("name"=>$key,"value"=>$value));
    	}
        $properties = array("properties"=>$properties);

        $associations = array_merge($associations, $properties);        
        $associations = json_encode($associations);

    	try{
    		return ($this->execute_JSON_post_request($this->get_request_url($endpoint,null),$associations));
    	} catch (HubSpot_Exception $e) {
    		throw new HubSpot_Exception('Unable to create contact: ' . $e);
    	}
    }

    /**
    * Search for a Deal
    *
    *@param params: 
    *
    * @return Response body with JSON object 
    * for created Contact from HTTP POST request
    *
    * @throws HubSpot_Exception
    **/
    public function get_recent_deals(){
    	$endpoint = 'deal/recent/created';
    	try{
    		return json_decode($this->execute_get_request($this->get_request_url($endpoint)));
    	}
    	catch(HubSpot_Exception $e){
    		throw new HubSpot_Exception('Unable to get contacts: '.$e);
    	}
    }

    /**
    * Associate deal with contact
    *
    * @param params: contactId and dealId
    *
    * @return 204 on successful 
    *
    * @throws HubSpot_Exception
    **/
    public function associate_deal_with_contact($dealId, $contactId){
    	$endpoint = 'deal/'.$dealId.'/associations/CONTACT?id='.$contactId.'&';
    	try{
    		return json_decode($this->execute_put_request($this->get_request1_url($endpoint)));
    	}
    	catch(HubSpot_Exception $e){
    		throw new HubSpot_Exception('Unable to get contacts: '.$e);
    	}
    }  
    
    public function associate_deal_with_company($dealId, $companyId){
    	$endpoint = 'deal/'.$dealId.'/associations/COMPANY?id='.$companyId.'&';
        //var_dump($this->get_request1_url($endpoint));
    	try{
    		return json_decode($this->execute_put_request($this->get_request1_url($endpoint)));
    	}
    	catch(HubSpot_Exception $e){
    		throw new HubSpot_Exception('Unable to get contacts: '.$e);
    	}        
    }
}

?>