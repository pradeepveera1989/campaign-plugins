<?php

	/* Function Name : copyDirectory
	 *
	 * Parameters : 
	 *    1. Source Path
	 *    2. Destination Path
	 *    3. Name of the splitTest
	 *
     * Returns : 
     *   
     */
	 
	function copyDirectory($source, $destination, $spTestName){

		# Creates destination directory 
		if(!is_dir($destination)){
			mkdir( $destination, 0777, true);
		}		
		$dir  = opendir($source);
		
		#Recursively copies all the files and folders.
		while(false !== ($file = readdir($dir))){
			if (( $file != '.' ) && ( $file != '..' )) {
				if ( is_dir($source . '/' . $file) ) {
					if($file !== $spTestName){
						copyDirectory($source . '/' . $file,$destination . '/' . $file);	
					}
				}
				else {
					copy($source . '/' . $file,$destination . '/' . $file);
				}
			}	
		}			
	}
	
	/* Function Name : modifyFile
	 *
	 * Parameters : 
	 *    1. config.ini file path for each Varient
	 *
     * Returns :  
	 *	  Success :  true
	 *    Failure :  false
     */	
	
	function modifyFile($file){
		$filecontent = file_get_contents($file, true);
		$dir = getcwd();
		$dir .= '/'.$file;
		$status = true;
		
		if(isset($filecontent)){
			$filecontent .= "\n\n[Varient] \n VarientFile = true \n";
		}else {
			var_dump("Unable to read the contents from config.ini from", $dir);
			$status = false;
		}
		if(!file_put_contents($file, $filecontent)){
			var_dump("Unable to write the contents to config.ini at", $dir);
			$status = false;
			
		}
		return $status;
	}
 
?> 