<?php
    /*
     * Round Robin Functionality
     * 
     * Description:
     * Get the value from database and check the count value. Accordingly does 
     * Round robin logic and update the database.
     * 1. Currently it redirects to all the pages equally.
     * 2. In future funtionality of dividing as per the percentage
     *
     */

	class splitTest{
		
		var $sql;
		var $spTestName;
		var $table;
		var $connect;
		var $result;
		var $currentIndex;
		var $varients;
		
		var $dbname = "db13026946-splittest";
		var $dbuser = "db13026946-sptst";
		var $dbpass = "9vnJ96rN";
		var $dbhost = "localhost";
		
		function __construct($name, $varients){
			$this->spTestName = $name;
			$this->varients = $varients;
			$this->tableName = "spTest_".$this->spTestName;
			$this->connect = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);
			if($this->connect->connect_errno){
				var_dump("Connection error to database: db13026946-splittest" );
			}		
		}

		/* Function Name : tableExists
		 *
		 * Parameters : Name of the table
		 *
         * Returns : 
         *    1. True if the table name exists in the databse.
         *    2. False if the table name does not exists in the datbase.
         */
		 
		public function tableExists(){
			$this->sql = "show tables like '$this->tableName'";
			$this->result = $this->connect->query($this->sql);
			if(($this->result->num_rows)){
				return true;
			}else{
				return false;
			}			
		}
		
		/* Function Name : createTable
		 *
		 * Parameters : Name of the table
		 *
         * Returns : 
         *    1. True if table is created in the databse.
         *    2. False if the table is not created in the databse.
         */
		 
		public function createTable(){
			$this->sql = "CREATE TABLE $this->tableName (
					id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
					splitTestValue INT(6)
					)";						
			$this->result = $this->connect->query($this->sql);
			var_dump($this->sql);
			if(!$this->result){
				var_dump("Unable to create the Database table for splitTest");
			}else {
				$this->sql = "INSERT INTO `$this->tableName` (`splitTestValue`) VALUES (1)";
				$this->result = $this->connect->query($this->sql);
				if(!$this->result){
					var_dump("Unable to insert value to Table");
				}
			}
		}

		/* 
		 * Function Name : getCurrentIndex
		 *
		 * Parameters : None
		 *
         * Returns : 
         *    1. current value of splitest value.
         */
		 		
		public function getCurrentIndex(){
			$this->sql = "SELECT * FROM  $this->tableName";
			$this->result = $this->connect->query($this->sql);
			if ($this->result->num_rows > 0) {
				while($row = $this->result->fetch_assoc()) {
					$this->currentIndex = $row["splitTestValue"];
				}
			}
			if(!$this->currentIndex){
				var_dump("Unable to fetch the current index from splitTest table");
			}
		}		

		/* 
		 * Function Name : getNewIndex
		 *
		 * Parameters : Current splitTest value
		 *
         * Returns : 
         *    1. Check the current value of splittest value
		 *    2. Generates a new splittest value.
		 *
         */
		 		
		public function getNewIndex($spTestIndex){
			if($this->currentIndex && $this->currentIndex < $this->varients){
				$this->newIndex = $this->currentIndex + 1;
			}else {
				$this->newIndex = 1;
			}
			$this->sql = "INSERT INTO `$this->tableName` (`splitTestValue`) VALUES ($this->newIndex)";
			$this->result = $this->connect->query($this->sql);
			if($this->result){
				$this->sql = "DELETE FROM `$this->dbname`.`$this->tableName` WHERE `$this->tableName`.`splitTestValue` = ($this->currentIndex)";
				$this->result = $this->connect->query($this->sql);
			}
			return ($this->newIndex);
		}			
		
		/* 
		 * Function Name : throwException
		 *
		 * Parameters : exception
		 *
         * Returns : 
         *    1. exception value.
         */
		private function throwException($e){
		
		}		
	}	
?>