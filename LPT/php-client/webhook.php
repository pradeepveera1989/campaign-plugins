<?php

    /*
    * @Name webhookIntegration
    *
    * @param params: 
	* 		$config_MailInOne:  MailInOne config parameters 
	*							Including array of  mail id's of recipient.
    *
    * @return 
	*		true on success and false on failure.
	*
    **/
	function webhookIntegration($config_MailInOne, $transactionId ,$standard_0, $standard_1, $standard_2, $standard_3, $standard_4, $custom_0, $custom_3, $email,$url){
		
		
		$transactionsService = new com_maileon_api_transactions_TransactionsService($config_MailInOne);
		$transactionsService->setDebug(false);	 
		 
		$transactions = array();
		
 		if(($config_MailInOne["CC"])){
			#var_dump($config_MailInOne["CC"]);
			// Create a transaction for each CC address
			foreach( $config_MailInOne["CC"] as $cc) { 
				$transaction = new com_maileon_api_transactions_Transaction();
				$transaction->contact = new com_maileon_api_transactions_ContactReference();
				$transaction->type = (int) $transactionId;					              
				$transaction->contact->email = $cc;

				# Custom fields respective to the adwords.1804 Contact Event  
				$transaction->content["email"] = $email;
				$transaction->content["SALUTATION"] = $standard_0;
				$transaction->content["FIRSTNAME"] = $standard_1;
				$transaction->content["LASTNAME"] = $standard_2;
				$transaction->content["ZIP"] = $standard_3;
				$transaction->content["CITY"] = $standard_4;
				$transaction->content["Quelle"] = $custom_0;
				$transaction->content["Telefon"] = $custom_3;  
				$transaction->content["url"] = $url;				

				// Only one transaction, here we could also submit some more transactions in one array
				$transactions = array($transaction);
				$response = $transactionsService->createTransactions($transactions, true, false);
			}
			
			// Validate return value
			if( $response->isSuccess() ) {
				// Transactions have been sent successfully; nothing else to do (except for logging maybe?)
				return true;
			} else {
				// An error occured while sending the transaction(s)
				// Add error handling (log the error, send a different transaction for an error notification etc.)
				return false;
			}					
		}else{
			return false;
		}	 
	}

	function contactEventIntegration($config_MailInOne, $transactionId, $standard_0, $standard_1, $standard_2, $standard_3, $standard_4, $custom_0, $custom_3, $email, $url, $vertriebemail, $vertriebsmitarbeiter){
		
		$transactionsService = new com_maileon_api_transactions_TransactionsService($config_MailInOne);
		$transactionsService->setDebug(false);	
		
		#TransactionService
		$transaction = new com_maileon_api_transactions_Transaction();
		$transaction->contact = new com_maileon_api_transactions_ContactReference();
		$transaction->type = (int) $transactionId;					              
		$transaction->contact->email = $email;

		# Custom fields respective to the Lead_Nurturing Contact Event		 
		$transaction->content["Anrede"] = $standard_0;
		$transaction->content["Vorname"] = $standard_1;
		$transaction->content["Nachname"] = $standard_2;
		$transaction->content["PLZ"] = $standard_3;
		$transaction->content["Ort"] = $standard_4;
		$transaction->content["Telefon"] = $custom_3;  
		$transaction->content["Vertriebsmitarbeiter"] = $vertriebsmitarbeiter;
		
		// Only one transaction, here we could also submit some more transactions in one array
		$transactions = array($transaction);

		$response = $transactionsService->createTransactions($transactions, true, false);
		if( $response->isSuccess() ) {
			// Transactions have been sent successfully; nothing else to do (except for logging maybe?)
			return true;
		} else {
			// An error occured while sending the transaction(s)
			// Add error handling (log the error, send a different transaction for an error notification etc.)
			return false;
		}	
	}

	
	
    /**
    * @Name updateContact
    *
    * @param params: 
	* 		$config_MailInOne:  MailInOne config parameters 
	*							Including array of  mail id's of recipient.
	*                           Other related fields of the form
    *
    * @return 
	*		response value.
	*
    **/
    function updateContact($email, $config_MailInOne, $standard_0, $standard_1, $standard_2, $standard_3, $standard_4, $custom_0, $custom_1, $custom_2, $custom_3, $leadqualitaet, $ip, $url, $trafficsource, $vertriebsmitarbeiter, $probetraining, $field=array(), $fieldvalue=array(), $hubspotcontactid, $hubspotdealid ,$urlparms, $resultCustomFields, $mapping) {
		
		$countoffields = sizeof($field);
  
        
        $debug = FALSE;
        $contactsService = new com_maileon_api_contacts_ContactsService($config_MailInOne);
        $contactsService->setDebug($debug);
        $newContact = new com_maileon_api_contacts_Contact();
        $newContact->email = $email;

        $newContact->standard_fields["SALUTATION"] = $standard_0;
        $newContact->standard_fields["FIRSTNAME"] = $standard_1;
        $newContact->standard_fields["LASTNAME"] = $standard_2;
        $newContact->standard_fields["ZIP"] = $standard_3;
        $newContact->standard_fields["CITY"] = $standard_4;
		$newContact->custom_fields["Vertriebsmitarbeiter"] = $vertriebsmitarbeiter;
        $newContact->custom_fields["Quelle"] = $custom_0;
        $newContact->custom_fields["Typ"] = $custom_1;
        $newContact->custom_fields["Segment"] = $custom_2;
        $newContact->custom_fields["Telefon"] = $custom_3;
		$newContact->custom_fields["Leadqualitaet"] = $leadqualitaet;
        $newContact->custom_fields["url"] = $url;
        $newContact->custom_fields["ip_adresse"] = $ip;
        $newContact->custom_fields["trafficsource"] = $trafficsource;
		$newContact->custom_fields["Probetraining"] = $probetraining;
        $newContact->custom_fields["FIBO2018"] = TRUE;
        $newContact->custom_fields["HubspotContactID"] = $hubspotcontactid;
        $newContact->custom_fields["HubspotDealID"] = $hubspotdealid;    
		while( $countoffields >= 0 ){
			if(isset($field[$countoffields])){
				$newContact->custom_fields[$field[$countoffields]] = $fieldvalue[$countoffields];
			}	
			$countoffields = $countoffields - 1;
		}	
        
        if(sizeof($urlparms) > 0 && sizeof($resultCustomFields) > 0){
            #Get the URL parameters
            var_dump($urlparms);
            foreach ($urlparms as $parm) {               
                $leftParm = explode("=", $parm)[0];
                # Search URL Parameters in Maileon custom fields
                if(array_key_exists($leftParm, $resultCustomFields)){
                    if(array_search($leftParm, $mapping)){  
                        $customField = array_search($leftParm, $mapping);   
                        #Update the new contact field with value.
                        var_dump($customField);
                        var_dump(explode("=", $parm)[1]);
                        $newContact->custom_fields[$customField] = explode("=", $parm)[1];
                    }       
                }
            }            
        }
        var_dump($newContact);
		#Update contact
        $response = $contactsService->createContact($newContact, com_maileon_api_contacts_SynchronizationMode::$UPDATE, "", "", false);
        return $response;
    }
?>
